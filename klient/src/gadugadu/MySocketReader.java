package gadugadu;

import java.io.BufferedReader;
import java.io.IOException;

/**
 * Created by Aleksander on 2017-12-26.
 */

public class MySocketReader {
    private BufferedReader bufferedReader;
    MySocketReader(BufferedReader bufferedReader) {
        this.bufferedReader = bufferedReader;
    }

    public String read() throws IOException{
        StringBuilder stringBuilder = new StringBuilder();

        int ch;

        while( (ch = this.bufferedReader.read()) != -1){
            if(ch == 10){
                break;
            }
            else{
                stringBuilder.append((char)ch);
            }
        }
        return stringBuilder.toString();
    }
}
