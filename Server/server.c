#include <stdio.h>
#include <stdlib.h>
#include <unistd.h>
#include <errno.h>
#include <string.h>
#include <pthread.h>
#include <sys/socket.h>
#include <netinet/in.h>
#include <arpa/inet.h>
#include <sys/types.h>
#include <stdbool.h>
#include <semaphore.h>


#define LIMIT_CLIENTS 150 //maksymalna liczba zarejestrowanych uzytkownikow
#define LIMIT_CONNECTIONS 150 //maksymalna liczba polaczonych uzytkownikow


sem_t sem_connections; //semafor na tablice connections;

sem_t sem_count; //semafor na zmienna registeredClients

struct clientData{
    int clientId;
    char clientName[15];
    bool ifOnline;
    char clientPassword[15];
    char path[50]; // path z zaleglymi wiadomosciami
}clients[LIMIT_CLIENTS];

struct clientConnections{
    struct sockaddr_in caddr; //adres klienta
    int connFd; //deskryptor
    int clientId;
};

struct clientConnections *connections[LIMIT_CONNECTIONS]; //lista aktywnych polaczen

int registeredClients; //zajestrowani klienci

void sendMessage(char *s, int fd)
{
    write(fd, s, strlen(s));
}


char* readMessage(int socket){
    char *buffer = malloc(1024);
    char tmp;
    int i = 0;
    int n;

    while(1){
        n = read(socket, &tmp, 1);
        if(n < 0){
            printf("Blad przy odczycie z socketa\n");
            return "Eror";
        }
        else{
            if(tmp == '\n'){
                buffer[i] = tmp;
                break;
            }
            buffer[i] = tmp;
            i=i+1;
        }
    }
    return buffer;
}
//zwraca id klienta, jesli nie znajdzie to zwraca o
int searchClient(char *name){
    printf("Szukany uzytkownik:%s/\n", name);
    int i;
    for(i=0; i<=registeredClients; i++){
        if(strcmp(clients[i].clientName, name)==0){
            return i;
        }
    }
    return 0;
}


//dopisuje do pliku nieodczytana wiadomosc
void writeMessage(int to_id, char *message){
    FILE *file;
    file = fopen(clients[to_id].path, "a");
    fprintf(file, "%s", message);
    fclose(file);
    printf("Dopisano do pliku %s\n", clients[to_id].path );
}

//wyswietla dane z tablicy klienci
void showClients(){
    int i;
    for(i=0; i<=registeredClients; i++){
        printf("%d: %s, %s\n", clients[i].clientId, clients[i].clientName, clients[i].clientPassword);
        if(clients[i].ifOnline==true) printf("ONLINE\n\n");
    }
}

//wyslij wiadomosc szukajac po id uzytkownika
void sendMessageId(char *s, int id)
{
    int i;
    bool send = false;
    for(i=0; i<LIMIT_CONNECTIONS; i++){
        if(connections[i]){
            if(connections[i]->clientId == id){
                sendMessage(s, connections[i]->connFd);
                send = true;
                break;
            }
        }
    }
    if (!send){
        writeMessage(id, s); //dopisanie do pliku z wiadomosciami do odczytu pozniejszego
    }
}


//Dodaje polaczenie do tablicy
void addConnection(struct clientConnections *c){
    int i;
    for(i=0;i<LIMIT_CONNECTIONS;i++){
        if(!connections[i]){
            sem_wait(&sem_connections);

            connections[i] = c;

            sem_post(&sem_connections);
            return ;
        }
    }
    printf("Dodano do polaczonych \n");
}

//Usuwa polaczenie z tablicy connections
void removeConnection(int id){
    int i;
    for(i=0;i<LIMIT_CONNECTIONS;i++){
        if(connections[i]){
            if(connections[i]->clientId == id){
                sem_wait(&sem_connections);

                connections[i] = NULL;

                sem_post(&sem_connections);
                return ;
            }
        }
    }

    printf("Usunieto z polaczonych\n");
}

//zwraca pozycje klienta w tablicy, jesli 0 to nie wyszukano
int checkLogin(char *name, char *password){
    printf("Login: %s\npass: %s\n", name, password );
    int i;
    for(i=0; i<=registeredClients; i++){
        if(strcmp(clients[i].clientName, name)==0)
        {
            if(strcmp(clients[i].clientPassword, password)==0)
                return i;
        } else {
            return 0;
        }
    }
    return 0;
}

//rejestracja klienta, 1 - gdy zarejestrowano nowego klienta, 0 - gdy klient istnieje
bool registerClient(char *name, char *password){
    if(searchClient(name)==0){
        sem_wait(&sem_count);

        registeredClients++;
        clients[registeredClients].clientId = registeredClients;
        strncpy(clients[registeredClients].clientName, name, sizeof(clients[registeredClients].clientName));
        strncpy(clients[registeredClients].clientPassword, password, sizeof(clients[registeredClients].clientPassword));

        //zapisanie sciezki do pliku do przechowywania zaleglych wiadomosci
        char file_path[50];
        strcpy(file_path, name);
        strcat(file_path, ".txt");
        strcpy(clients[registeredClients].path, file_path);
        //utworzenie pliku przechowywujacego zalegle wiadomosci:
        FILE *file;
        file = fopen(clients[registeredClients].path, "a");
        fclose(file);

        sem_post(&sem_count);

        printf("Zarejestrowano uzytkownika %s, haslo: %s\n", name, password);
        return 1;
    }
    else{
        printf("Uzytkownik juz istnieje\n");
        return 0;
    }
}
void checkFriendStatus(int *friends, int n, int fd){
    int i;
    int j;
    char message[1024];
    char tmp[1024];
    strcpy(message, "#STATUS");
    for (i=0; i<n; i++){
        for(j=1; j<=registeredClients; j++){ 
            if(friends[i]==clients[j].clientId){
                if(clients[j].ifOnline==true){
                    sprintf(tmp, "%d", clients[j].clientId);
                    strcat(message, " ");
                    strcat(message, tmp);
                }
            }
        }
    }
    strcat(message, "\n");
    sendMessage(message, fd);
}
void sendOldMessages(int id){
    FILE *file;
    char line[1024];
    file = fopen(clients[id].path, "r");
    while (1) {
        if (fgets(line,1024, file) == NULL) break;
        sendMessageId(line, id);
    }
    freopen(clients[id].path, "w", file); 
    fclose(file);
}

void stripNewline(char *s){
    while(*s != '\0'){
    if(*s == '\r' || *s == '\n'){
        *s - '\0'; }
    s++;
    }
}

//obsluga klienta w osobnym watku
void* cthread(void* arg){
    char *buff_in;
    char message[1024];
    int tmp=0;
    int clientId=0;
    struct clientConnections* c = (struct clientConnections*)arg;

    printf("Udalo sie nawiazac polaczenie z klientem\n");
    //przyjmuj dane od klienta
    while(1){
        buff_in = readMessage(c->connFd); //odczyt wiadomosci od klienta
        stripNewline(buff_in);
        strcpy(message, "");
        printf("wiadomosc: %sx\n", buff_in);
        //wszystkie wiadomosci powinny zaczynac sie od znaku '#'
        if(buff_in[0]== '#'){
            char *command, *param;
            command = strtok(buff_in, " ");

            char name[15];
            char password[15];

            //ZALOGUJ SIE
            if(strcmp(command, "#LOGIN")==0){ // #LOGIN clientName userPassword
                param = strtok(NULL, " "); //rozpoczyna przeszukiwanie znakow
                if(param){
                    strcpy(name, param);
                    param = strtok(NULL, " ");
                    if(param){
                        strcpy(password, param);
                    }
                }
                if ((tmp = checkLogin(name, password)) > 0){
                    //zalogowano uzytkownika
                    clientId = tmp;
                    clients[clientId].ifOnline = true;

                    c->clientId = clients[clientId].clientId; //przypisanie id tego uzytkownika do polaczenia
                    addConnection(c);

                    char tmp_message[1024];
                    char s_id[1024];
                    sprintf(s_id, "%d", clientId);

                    strcpy(tmp_message, "#OK ");
                    strcat(tmp_message, s_id);
                    strcat(tmp_message, "\n");

                    printf("Zalogowano uzytkownika\n");
                    sendMessage(tmp_message, c->connFd);
                    printf("Przy lgoowaniu wyslano: %s\n", tmp_message );

                    sendOldMessages(clientId);
                }
                else{
                    printf("Nie udalo sie zalogowac uzytkownika\n");
                    sendMessage("#ERR\n", c->connFd);
                }
                printf("tmp: %d\n",tmp);
                printf("client Id: %d\n",clientId);
            }
                //WYLOGUJ SIE
            else if(strcmp(command, "#LOGOUT")==0){
                printf("Wylogowanie\n");
                clients[clientId].ifOnline = false;
                removeConnection(clients[clientId].clientId); //usuniece polaczenia z tablicy polaczen
                break;
            }

                //ZAREJESTRUJ
            else if(strcmp(command, "#REGISTER")==0){	//#REGISTER clientName userPassword
                param = strtok(NULL, " ");
                if(param){
                    strcpy(name, param);
                    param = strtok(NULL, "");
                    if(param){
                        strcpy(password, param);

                        if(registerClient(name, password)){ // poprawnie zarejestowano
                            showClients();
                            sendMessage("#OK\n", c->connFd);
                        }
                        else{
                            sendMessage("#ERR\n", c->connFd);
                        }
                    }
                }

            }
                //WYSZUKIWANIE UZYTKOWNIKA O PODANEJ NAZWIE
            else if(strcmp(command, "#SEARCH")==0){ //#SEARCH clientName
                int searched_id=0;
                char tmp[1024];
                char s_id[1024];
                param = strtok(NULL, " ");

                if(param){
                    if ((searched_id= searchClient(param)) > 0){
                        sprintf(s_id, "%d", searched_id);

                        strcpy(tmp, "#OK ");
                        strcat(tmp, s_id);
                        strcat(tmp, "\n");

                        sendMessage(tmp, c->connFd);
                    }
                    else{
                        sendMessage("#ERR\n", c->connFd);
                    }
                }
            }
             //SPRAWDZ STATUSY
            else if(strcmp(command, "#STATUS")==0){ //#STATUS id1 id2...
                printf("Sprawdz statusy\n");

                int friends[150];
                int n=0;

                param = strtok(NULL, " ");
                if(param){
                    while(param!=NULL){
                        friends[n]=atoi(param);
                        param = strtok(NULL, " ");
                        n++;
                    }
                }
                checkFriendStatus(friends, n, c->connFd); //sprawdzenie statusow i odeslanie do klienta id uzytkownikow, ktorzy sa ifOnline
            }

            //PRZESYLANIE WIADOMOSCI
            else if(strcmp(command, "#MESSAGE")==0){ //#MESSAGE do_kogo_userId od_kogo_userId tresc
                printf("Wiadomosc:\n");
                param = strtok(NULL, " ");
                if (param){
                    int to_id = atoi(param); //clientId adresata

                    param = strtok(NULL, " ");
                    if(param){
                        sprintf(message, "%s", "#MESSAGE ");
                        strcat(message, param);	//dodanie do wiadomosci clientId nadawcy
                        param = strtok(NULL, " ");
                        if (param){
                            while(param!=NULL){
                                strcat(message, " ");
                                strcat(message, param);
                                param = strtok(NULL, " ");
                            }
                            strcat(message, "\r\n");
                            sendMessageId(message, to_id); //#MESSAGE od_kogo wiadomosc
                        }
                    }
                }
                printf("%s\n", message);
            }
            else{
                printf("Blad. Niepoprawna komenda!\n");
                sendMessage("#ERR\n", c->connFd);
            }
        }
    }

    close(c->connFd);
    free(c);

    return 0;
}
int main(int argc, char **argv)
{

    registeredClients = 0;

    sem_init(&sem_count, 0, 1);
    sem_init(&sem_connections, 0, 1);

    pthread_t tid;
    socklen_t slt;

    struct sockaddr_in saddr;

    int listener = socket(AF_INET, SOCK_STREAM, 0);

    int on = 1;
    setsockopt(listener, SOL_SOCKET, SO_REUSEADDR, (char*)&on, sizeof(on));

    saddr.sin_family = AF_INET;
    saddr.sin_addr.s_addr = htonl(INADDR_ANY);
    saddr.sin_port = htons(5001);

    // laczy adres z gniazdem
    if(bind(listener, (struct sockaddr*)&saddr, sizeof(saddr)) < 0){
        perror("Socket binding failed");
        return 1;
    }

    //listen - gniazdo pasywne, wiec akceptuje wszystkie polaczenia
    if(listen(listener, 10) < 0){
        perror("Socket listening failed");
        return 1;
    }

    //przyjmowanie polaczen od klientow
    while(1)
    {
        struct clientConnections* c = malloc(sizeof(struct clientConnections));
        slt = sizeof(c->caddr);
        c->connFd = accept(listener, (struct sockaddr*)&c->caddr, &slt);
        pthread_create(&tid, NULL, cthread, c);
        pthread_detach(tid);
    }

    return 0;
}